<?
    /**
	 * Elgg SAML authentication
	 *
	 * @package ElggSAMLAuth
	 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
	 * @author Jerome Schneider <jschnneider@entrouvert.com>
	 */

$classical_auth_saml_string = elgg_echo('saml_auth:settings:label:classical_auth');
$classical_auth_saml_view = elgg_view('input/dropdown', array(
    'name' => 'params[classical_auth]',
    'options_values' => array(
        'yes' => elgg_echo('option:yes'),
        'no' => elgg_echo('option:no'),
        ),
    'value' => $vars['entity']->classical_auth ? $vars['entity']->classical_auth : 'yes',
));

?>

<p>
    <fieldset style="border: 1px solid; padding: 15px; margin: 0 10px 0 10px">
        <legend><?php echo elgg_echo('saml_auth:settings:label:simplesamlphp');?></legend>

        <label for="params[sp_name]"><?php echo elgg_echo('saml_auth:settings:label:sp_name');?></label><br/>
        <div class="example"><?php echo elgg_echo('saml_auth:settings:help:sp_name');?></div>
        <input type="text" name="params[sp_name]" value="<?php echo $vars['entity']->sp_name;?>"/><br/>
        <label><?php print $classical_auth_saml_string; ?></label> <?php print $classical_auth_saml_view  ?>
    </fieldset>
    <fieldset style="border: 1px solid; padding: 15px; margin: 0 10px 0 10px">
        <legend><?php echo elgg_echo('saml_auth:settings:label:attributes');?></legend>

        <label for="params[username]"><?php echo elgg_echo('saml_auth:settings:label:username');?></label><br/>
        <input type="text" name="params[username]" value="<?php echo $vars['entity']->username;?>"/><br/>
        <label for="params[firstname]"><?php echo elgg_echo('saml_auth:settings:label:firstname');?></label><br/>
        <input type="text" name="params[firstname]" value="<?php echo $vars['entity']->firstname;?>"/><br/>
        <label for="params[surname]"><?php echo elgg_echo('saml_auth:settings:label:surname');?></label><br/>
        <input type="text" name="params[surname]" value="<?php echo $vars['entity']->surname;?>"/><br/>
        <label for="params[email]"><?php echo elgg_echo('saml_auth:settings:label:email');?></label><br/>
        <input type="text" name="params[email]" value="<?php echo $vars['entity']->email;?>"/><br/>
    </fieldset>
</p>
