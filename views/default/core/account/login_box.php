<?php
/**
 * Elgg login box
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['module'] The module name. Default: aside
 */

$SAML = true;

try {
    $as = new SimpleSAML_Auth_Simple(elgg_get_plugin_setting('sp_name', 'saml_auth'));
} catch (Exception $e) {
    $SAML = false;
    register_error(elgg_echo('saml_auth:samlerror'));
}
if (array_key_exists('login', $_REQUEST))
{
    try {
        $as->requireAuth();
    } catch (Exception $e) {
        $SAML = false;
        register_error(elgg_echo('saml_auth:samlerror'));
    }
}
$isAuth = $as->isAuthenticated();

$module = elgg_extract('module', $vars, 'aside');

$title = elgg_echo('login');

$login_url = elgg_get_site_url();
if (elgg_get_config('https_login')) {
    $login_url = str_replace("http:", "https:", $login_url);
}

$body = "";

if ($SAML == true)
{
    $body .= elgg_view_form('login_saml', array('action' => "{$login_url}"));
    $body .= '<br />';
}
if (($SAML == false) or (elgg_get_plugin_setting('classical_auth', 'saml_auth') == 'yes'))
{
    $body .= elgg_view_form('login', array('action' => "{$login_url}action/login"));
}

echo elgg_view_module($module, $title, $body);

