<?php
/**
 * Elgg SAML v2.0 authentication
 *
 * @package ElggSAMLAuth
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
 * @author Jerome Schneider <jschneider@entrouvert.com>
 */
?>

<div id="login-box">
    <p><?php echo elgg_echo('saml_auth:account:authentication:text') ?></p>
    <input type="hidden" value="1" name="login">
    <input type="submit" value="<? echo elgg_echo('saml_auth:account:authentication:button') ?>" class="submit_button" name="">
</div>

