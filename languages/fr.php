<?php

    /**
     * Elgg SAML authentication
     *
     * @package ElggSAMLAuth
     * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
     * @author Jerome Schneider <jschneider@entrouvert.com>
     */

    $fr = array(
        'saml_auth:settings:label:simplesamlphp' => "Configuration de SimpleSAMLphp",
        'saml_auth:settings:label:sp_name' => "Nom du fournisseur de service",
        'saml_auth:settings:help:sp_name' => "Nom de votre fournisseur de service SimpleSAMLphp",
        'saml_auth:settings:label:attributes' => "Attributs",
        'saml_auth:settings:label:username' => "Nom d'utilisateur",
        'saml_auth:settings:label:firstname' => "Prénom",
        'saml_auth:settings:label:surname' => "Nom",
        'saml_auth:settings:label:email' => "Courriel",
        'saml_auth:settings:label:classical_auth' => "Autoriser l'auhtentification classique",
        'saml_auth:account:authentication:text' => "Cliquer sur le bouton pour vous connecter via UnivCloud",
        'saml_auth:account:authentication:button' => "Se connecter avec UnivCloud",
        'saml_auth:samlerror' => "Le plugin SAML n'est pas configuré correctement. Il n'est pas utilisé.",
	'saml_auth:errorattrs' => "Nom d'utilisateur ou / et email inconnu"
    );

    add_translation('fr', $fr);
?>
