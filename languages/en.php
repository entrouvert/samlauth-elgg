<?php

    /**
     * Elgg SAML authentication
     *
     * @package ElggSAMLAuth
     * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
     * @author Jerome Schneider <jschneider@entrouvert.com>
     */

    $en = array(
        'saml_auth:settings:label:simplesamlphp' => "SimpleSAMLphp configuration",
        'saml_auth:settings:label:sp_name' => "Service Provider name",
        'saml_auth:settings:help:sp_name' => "The name of your SP in SimpleSAMLphp",
        'saml_auth:settings:label:attributes' => "Attributes mapping",
        'saml_auth:settings:label:username' => "Username",
        'saml_auth:settings:label:firstname' => "Firstname",
        'saml_auth:settings:label:surname' => "Surname",
        'saml_auth:settings:label:email' => "Email address",
        'saml_auth:settings:label:classical_auth' => "Enable classical authentification",
        'saml_auth:account:authentication:text' => "Please click on the button to connect with UnivCloud",
        'saml_auth:account:authentication:button' => "Log In with UnivCloud",
        'saml_auth:samlerror' => "The SAML plugin is misconfigured. It will not be used.",
	'saml_auth:errorattrs' => "Username or / and email not transmited"
    );

    add_translation('en', $en);
?>
